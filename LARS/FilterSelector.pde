class FilterSelector {
  protected int scale;
  
  protected PImage icon;
  protected int posX, posYWall;
  protected int iconSize = (int)(1.3 * oneMeter);
 
  protected int startYWall;
  protected int activationSizeX = 2 * oneMeter;
  protected int activationSizeY = oneMeter;
  
  protected Filter filter;
  
  
  public FilterSelector(Filter filter, int startYWall, int scale) {
    this.filter = filter;
    this.startYWall = startYWall;
    this.scale = scale;
  }
  
  
  public void setIcon(String iconPath) {
    this.icon = loadImage(iconPath);
  }
  
  public void setPosition(int x, int yWall) {
    this.posX = x;
    this.posYWall = yWall;
  }
  
  public void checkAction(Actor actor) {
    if (actor.getPosX() >= this.posX - this.activationSizeX / 2 / this.scale && actor.getPosX() <= this.posX + this.activationSizeX / 2 / this.scale && actor.getPosY() <= this.startYWall + this.activationSizeY / this.scale) {
      actor.setFilter(this.filter);
    }
  }
  
  public void draw() {
    // draw instruction text
    fill(124, 197, 128);
    text("Schnapp dir einen Filter. Gehe zu einer aktivierten Spur. Modifiziere sie.", 1260 / this.scale, 1550 / this.scale);
    
    // draw icon
    if (this.filter != null) {
      tint(124, 197, 128);
    } else {
      tint(212, 97, 90);
    }
    image(this.icon, this.posX - (this.iconSize / 2) / this.scale, this.posYWall - (this.iconSize / 2) / this.scale, this.iconSize / this.scale, this.iconSize / this.scale);
    
    // draw selector (ground)
    noStroke();
    if (this.filter != null) {
      fill(124, 197, 128, 50);
    } else {
      fill(212, 97, 90, 50);
    }
    arc(this.posX, this.startYWall, this.activationSizeX / this.scale, this.activationSizeY * 2 / this.scale, 0, PI);
    
    // write filter-name (ground)
    noStroke();
    fill(0, 15, 30);
    if (this.filter != null) text(this.filter.getFilterName(), this.posX, this.startYWall + 70 / this.scale);
  }
}