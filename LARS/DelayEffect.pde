class DelayEffect extends Filter {
  
  protected Delay filter;
  protected UGenSetter ugs;
  
  public DelayEffect(String name, float sampleRate, int scale) {
    super(name, sampleRate, scale);
    this.filterType = 2;
    this.filter = new Delay(.5f, 0f, true, true);
    this.ugs = new UGenSetter(this.filter);
  }
  
  public UGen getFilterReference() {
    return this.filter;
  }
  
  public void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange) {
    float delay = map(localX, doubleMinRange / this.scale / 2f, doubleMaxRange / this.scale / 2f, .05f, .5f);
    float amplitude = map(localY, doubleMaxRange / this.scale / 2f, -doubleMaxRange / this.scale / 2f, 0f, 1f);
    if (delay < .05f) delay = .05f;
    if (delay > .5f) delay = .5f;
    if (amplitude < 0) amplitude = 0;
    if (amplitude > 1) amplitude = 1;
    ugs.setDelayUGen(delay);
    ugs.setAmplitudeUGen(amplitude);
    this.filterParameters = "Feedback: " + (int)(amplitude * 100) + "%\nDelay: " + (int)(delay * 1000) + "ms";
    this.showFilterParameters = true;
  }
  
  public void setFilterNoEffect() {
    ugs.setDelayUGen(.5f);
    ugs.setAmplitudeUGen(0f);
    this.showFilterParameters = false;
  }
  
  public class UGenSetter extends UGen {
    protected Delay filter;
    
    public UGenSetter(Delay filter) {
      this.filter = filter;
    }
    public void uGenerate(float[] input) { }
    
    public void setDelayUGen(float value) {
      this.filter.delTime = new UGen.UGenInput(UGen.InputType.CONTROL, value);
    }
    
    public void setAmplitudeUGen(float value) {
      this.filter.delAmp = new UGen.UGenInput(UGen.InputType.CONTROL, value);
    }
  }
}