import ddf.minim.effects.*;

abstract class Filter {
  protected int scale;
  
  protected String name;
  
  protected float sampleRate;
  
  protected PImage icon;
  protected int iconSize = (int)(.4 * oneMeter);
  
  protected int iconAmount = 4;
  protected int iconAngleDist = (int)(.6 * oneMeter);
  protected float iconAngleVel = PI / 4;
  protected float iconAngle = 0f;
  
  protected boolean showFilterParameters = false;
  protected String filterParameters = "";
  
  protected int filterType = -1;
  
  public Filter(String name, float sampleRate, int scale) { 
    this.name = name;
    this.sampleRate = sampleRate;
    this.scale = scale;
  }
  
  public int getFilterType() {
    return this.filterType;
  }
  
  public String getFilterName() {
    return this.name;
  }
  
  public String getFilterParameters() {
    if (!showFilterParameters) return "";
    return filterParameters;
  }
  
  public void setIcon(String iconPath) {
    this.icon = loadImage(iconPath);
  }
  
  public void updateRotationIcon(float deltaTime) {
    this.iconAngle += iconAngleVel * deltaTime;
  }
  
  public abstract UGen getFilterReference();
  public abstract void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange);
  public abstract void setFilterNoEffect();
  
  public void draw(int x, int y) {
    tint(124, 197, 118);
    for (int i = 0; i < iconAmount; i++) {
      float angleOffsetX = iconAngleDist / this.scale * cos(iconAngle + i * 2 * PI / this.iconAmount);
      float angleOffsetY = iconAngleDist / this.scale * sin(iconAngle + i * 2 * PI / this.iconAmount);
      image(this.icon, x + angleOffsetX - (this.iconSize / 2) / this.scale, y + angleOffsetY - (this.iconSize / 2) / this.scale, this.iconSize / this.scale, this.iconSize / this.scale);
    }
  }
}