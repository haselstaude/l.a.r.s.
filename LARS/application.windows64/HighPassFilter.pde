class HighPassFilter extends Filter {
  
  protected HighPassSP filter;
  
  public HighPassFilter(String name, float sampleRate, int scale) {
    super(name, sampleRate, scale);
    this.filterType = 1;
    this.filter = new HighPassSP(1, this.sampleRate);
  }
  
  public UGen getFilterReference() {
    return this.filter;
  }
  
  public void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange) {
    float frequency = map(localX, doubleMinRange / this.scale / 2f, doubleMaxRange / this.scale / 2f, 200, 5000);
    if (frequency < 200) frequency = 200;
    if (frequency > 5000) frequency = 5000;
    this.filter.setFreq(frequency);
    this.filterParameters = "Cutoff: " + (int)frequency + " Hz";
    this.showFilterParameters = true;
  }
  
  public void setFilterNoEffect() {
    this.filter.setFreq(1);
    this.showFilterParameters = false;
  }
}