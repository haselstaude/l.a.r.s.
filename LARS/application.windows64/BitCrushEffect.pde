class BitCrushEffect extends Filter {
  
  protected BitCrush filter;
  
  public BitCrushEffect(String name, float sampleRate, int scale) {
    super(name, sampleRate, scale);
    this.filterType = 4;
    this.filter = new BitCrush(16, 48000);
  }
  
  public UGen getFilterReference() {
    return this.filter;
  }
  
  public void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange) {
    float bitRes = map(localX, doubleMinRange / this.scale / 2f, doubleMaxRange / this.scale / 2f, 16f, 1f);
    if (bitRes < 1f) bitRes = 1f;
    if (bitRes > 16f) bitRes = 16f;
    this.filter.setBitRes(bitRes);
    this.filterParameters = "Resolution: " + (int)bitRes + "bit";
    this.showFilterParameters = true;
  }
  
  public void setFilterNoEffect() {
    this.filter.setBitRes(16f);
    this.showFilterParameters = false;
  }
}