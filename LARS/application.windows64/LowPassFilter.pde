class LowPassFilter extends Filter {
  
  protected LowPassSP filter;
  
  public LowPassFilter(String name, float sampleRate, int scale) {
    super(name, sampleRate, scale);
    this.filterType = 0;
    this.filter = new LowPassSP(20000, this.sampleRate);
  }
  
  public UGen getFilterReference() {
    return this.filter;
  }
  
  public void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange) {
    float frequency = map(localX, doubleMinRange / this.scale / 2f, doubleMaxRange / this.scale / 2f, 1000, 10);
    if (frequency < 10) frequency = 10;
    if (frequency > 1000) frequency = 1000;
    this.filter.setFreq(frequency);
    this.filterParameters = "Cutoff: " + (int)frequency + " Hz";
    this.showFilterParameters = true;
  }
  
  public void setFilterNoEffect() {
    this.filter.setFreq(20000);
    this.showFilterParameters = false;
  }
}