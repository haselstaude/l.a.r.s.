class FlangerEffect extends Filter {
  
  protected ddf.minim.ugens.Flanger filter;
  protected UGenSetter ugs;
  
  public FlangerEffect(String name, float sampleRate, int scale) {
    super(name, sampleRate, scale);
    this.filterType = 3;
    this.filter = new ddf.minim.ugens.Flanger(1, .2f, 1, 0f, .5f, .0f);
    this.ugs = new UGenSetter(this.filter);
  }
  
  public UGen getFilterReference() {
    return this.filter;
  }
  
  public void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange) {
    float wet = map(localX, doubleMinRange / this.scale / 2f, doubleMaxRange / this.scale / 2f, 0f, 1f);
    float feedback = map(localY, doubleMaxRange / this.scale / 2f, -doubleMaxRange / this.scale / 2f, 0f, 1f);
    if (wet < 0f) wet = 0f;
    if (wet > 1f) wet = 1f;
    if (feedback < 0) feedback = 0;
    if (feedback > 1) feedback = 1;
    ugs.setWetUGen(wet);
    ugs.setFeedbackUGen(feedback);
    this.filterParameters = "Feedback: " + (int)(feedback * 100) + "%\nWet: " + (int)(wet * 100) + "%";
    this.showFilterParameters = true;
  }
  
  public void setFilterNoEffect() {
    ugs.setWetUGen(.0f);
    ugs.setFeedbackUGen(0f);
    this.showFilterParameters = false;
  }
  
  public class UGenSetter extends UGen {
    protected ddf.minim.ugens.Flanger filter;
    
    public UGenSetter(ddf.minim.ugens.Flanger filter) {
      this.filter = filter;
    }
    public void uGenerate(float[] input) { }
    
    public void setWetUGen(float value) {
      this.filter.wet = new UGen.UGenInput(UGen.InputType.CONTROL, value);
    }
    
    public void setFeedbackUGen(float value) {
      this.filter.feedback = new UGen.UGenInput(UGen.InputType.CONTROL, value);
    }
  }
}