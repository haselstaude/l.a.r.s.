import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import ddf.minim.*; 
import ddf.minim.effects.*; 
import TUIO.*; 
import java.util.Vector; 
import ddf.minim.*; 
import ddf.minim.ugens.*; 
import ddf.minim.spi.*; 
import java.util.Iterator; 

import TUIO.*; 
import com.illposed.osc.*; 
import com.illposed.osc.utility.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class LARS extends PApplet {




// tracking
boolean pcDebugging = false;    // true: mouse used for tracking and scaled output, false: TUIO-tracking and fullscreen output
int pcDebuggingControl = 0;
ArrayList<Actor> actors;
int fpsTracking = 20;
int timeLastTrack;

// audio
Minim minim;
Track[] tracks;
int amountTracks = 8;
Filter[] filters;
FilterSelector[] filterSelectors;
int amountFilters = 5;

// rendering
PFont fontTitle;
PFont fontGeneral;
int scale = 4;
int windowWidth = 3030 / scale;
int windowHeight = 3712 / scale;
int wallHeight = 1914 / scale;
int oneMeter = (3712 - 1914) / 9;
BarVisualizer barVisualizer;

public void settings() {
  size(windowWidth, windowHeight);
  if (!pcDebugging) fullScreen();
}



public void setup() {
  // fonts
  fontTitle = loadFont("ShareTechMono.vlw");
  fontGeneral = loadFont("Consolas.vlw");
  textAlign(CENTER, TOP);
  
  // actors
  actors = new ArrayList<Actor>();
  if (pcDebugging) actors.add(new Actor(pcDebuggingControl, scale));
  
  // elements
  minim = new Minim(this);
  tracks = new Track[amountTracks];
  filters = new Filter[amountFilters];
  filterSelectors = new FilterSelector[amountFilters + 1];
  setupTracks();
  delay(1000);
  startTracks();
  setupFilter();
  setupFilterSelection();
  
  // bar-visualizer
  barVisualizer = new BarVisualizer();
  for (int i = 0; i < amountTracks; i++) {
    barVisualizer.addTrackReference(tracks[i]);
  }
  
  // tracking
  if (!pcDebugging) initTracking(false, wallHeight);
}



public void setupTracks() {
  tracks[0] = new Track(minim, "tracks/audio/Electro_Drum.wav", scale, false);
  tracks[0].setIcon("tracks/icon/Electro_Drum.png");
  tracks[0].setPosition(366 / scale, wallHeight + 600 / scale);
  
  tracks[1] = new Track(minim, "tracks/audio/Noise.wav", scale, false);
  tracks[1].setIcon("tracks/icon/Noise.png");
  tracks[1].setPosition(1066 / scale, wallHeight + 600 / scale);
  
  tracks[2] = new Track(minim, "tracks/audio/Melodic.wav", scale, false);
  tracks[2].setIcon("tracks/icon/Melodic.png");
  tracks[2].setPosition(1766 / scale, wallHeight + 600 / scale);
  
  tracks[3] = new Track(minim, "tracks/audio/Slow_Drum.wav", scale, false);
  tracks[3].setIcon("tracks/icon/Slow_Drum.png");
  tracks[3].setPosition(2466 / scale, wallHeight + 600 / scale);
  
  tracks[4] = new Track(minim, "tracks/audio/Trance.wav", scale, true);
  tracks[4].setIcon("tracks/icon/Trance.png");
  tracks[4].setPosition(432 / scale, wallHeight + 1350 / scale);
  
  tracks[5] = new Track(minim, "tracks/audio/Octave.wav", scale, true);
  tracks[5].setIcon("tracks/icon/Octave.png");
  tracks[5].setPosition(1132 / scale, wallHeight + 1350 / scale);
  
  tracks[6] = new Track(minim, "tracks/audio/Fast_Drum.wav", scale, true);
  tracks[6].setIcon("tracks/icon/Fast_Drum.png");
  tracks[6].setPosition(1832 / scale, wallHeight + 1350 / scale);
  
  tracks[7] = new Track(minim, "tracks/audio/Guitar.wav", scale, true);
  tracks[7].setIcon("tracks/icon/Guitar.png");
  tracks[7].setPosition(2532 / scale, wallHeight + 1350 / scale);
}

public void setupFilter() {  
  filters[0] = new LowPassFilter("Tief-Pass", tracks[0].getSampleRate(), scale);
  filters[0].setIcon("filters/icon/lowpass.png");
  
  filters[1] = new HighPassFilter("Hoch-Pass", tracks[0].getSampleRate(), scale);
  filters[1].setIcon("filters/icon/highpass.png");
  
  filters[2] = new DelayEffect("Delay", tracks[0].getSampleRate(), scale);
  filters[2].setIcon("filters/icon/reverb.png");
  
  filters[3] = new FlangerEffect("Flanger", tracks[0].getSampleRate(), scale);
  filters[3].setIcon("filters/icon/flanger.png");
  
  filters[4] = new BitCrushEffect("BitCrush", tracks[0].getSampleRate(), scale);
  filters[4].setIcon("filters/icon/distortion.png");
}

public void setupFilterSelection() {
  filterSelectors[0] = new FilterSelector(filters[0], wallHeight, scale);
  filterSelectors[0].setIcon("filters/icon/lowpass.png");
  filterSelectors[0].setPosition(250 / scale, 1750 / scale);
  
  filterSelectors[1] = new FilterSelector(filters[1], wallHeight, scale);
  filterSelectors[1].setIcon("filters/icon/highpass.png");
  filterSelectors[1].setPosition(750 / scale, 1750 / scale);
  
  filterSelectors[2] = new FilterSelector(filters[2], wallHeight, scale);
  filterSelectors[2].setIcon("filters/icon/reverb.png");
  filterSelectors[2].setPosition(1250 / scale, 1750 / scale);
  
  filterSelectors[3] = new FilterSelector(filters[3], wallHeight, scale);
  filterSelectors[3].setIcon("filters/icon/flanger.png");
  filterSelectors[3].setPosition(1750 / scale, 1750 / scale);
  
  filterSelectors[4] = new FilterSelector(filters[4], wallHeight, scale);
  filterSelectors[4].setIcon("filters/icon/distortion.png");
  filterSelectors[4].setPosition(2250 / scale, 1750 / scale);
  
  filterSelectors[5] = new FilterSelector(null, wallHeight, scale);
  filterSelectors[5].setIcon("filters/icon/nofilter.png");
  filterSelectors[5].setPosition(2750 / scale, 1750 / scale);
}

public void startTracks() {
  for (int i = 0; i < amountTracks; i++) {
    this.tracks[i].startStreaming();
  }
}



public void draw() {
  int updateStart = millis();
  // update
  if (millis() - timeLastTrack >= 1000 / fpsTracking) {
    updateTrackingData();
    timeLastTrack = millis();
  }
  updateFilters();
  updateTracks();
  int updateEndDrawStart = millis();
  int updateDuration = updateEndDrawStart - updateStart;
  
  // draw
  drawRoom();
  drawInterface();
  drawFilterSelectors();
  drawTracks();
  drawActors();
  drawBarVisualizer();
  int drawDuration = millis() - updateEndDrawStart;
  
  // FPS
  surface.setTitle("L.A.R.S. | " + (int)frameRate + " FPS | Update: " + updateDuration + "ms | Draw: " + drawDuration + "ms");
  textFont(fontGeneral, 36 / scale);
  textAlign(RIGHT, TOP);
  text((int)frameRate + " FPS | Update: " + updateDuration + "ms | Draw: " + drawDuration + "ms", windowWidth - 50 / scale, 50 / scale); 
  textAlign(CENTER, TOP);
  textFont(fontGeneral, 54 / scale);
}



public void updateTrackingData() {
  if (pcDebugging) {
    actors.get(pcDebuggingControl).syncTracking(mouseX, mouseY);
  } else {
    ArrayList<Actor> newActorList = new ArrayList<Actor>();
    
    // iterate through tracking-tracks
    for (int trackId = 0; trackId < GetNumTracks(); trackId++) {
      Actor knownActor = getActor(GetCursorID(trackId));
      Actor newActor = new Actor(GetCursorID(trackId), scale);
      if (knownActor != null) {
        // known actor -- copy filter
        newActor.setFilter(knownActor.getFilter());
      }
      newActor.syncTracking(GetX(trackId), GetY(trackId));
      newActorList.add(newActor);
    }
    
    actors = newActorList;
  }
}

public Actor getActor(int cursorId) {
  Iterator<Actor> iterator = actors.iterator();
  while (iterator.hasNext()) {
    Actor actor = iterator.next();
    if (actor.getCursorId() == cursorId) {
      return actor;
    }
  }
  return null;
}



public void updateFilters() {
  // filter-selectors
  for (int i = 0; i < amountFilters + 1; i++) {
    if (i < amountFilters) 
      filters[i].updateRotationIcon(1f / frameRate);
      
    Iterator<Actor> iterator = actors.iterator();
    while (iterator.hasNext()) {
      filterSelectors[i].checkAction(iterator.next());
    }
  }
}

public void updateTracks() {
  for (int i = 0; i < amountTracks; i++) {
    tracks[i].updateFilters();
    tracks[i].updateVolume();
    Iterator<Actor> iterator = actors.iterator();
    while (iterator.hasNext()) {
      tracks[i].checkAction(iterator.next());
    }
    tracks[i].updateRotationWave(1f / frameRate);
  }
}



public void drawRoom() {
  background(255);
  
  // wall
  noStroke();
  fill(0, 15, 30);
  rect(0, 0, windowWidth, wallHeight);
  
  // floor
  noStroke();
  fill(0, 15, 30);
  rect(0, wallHeight, windowWidth, windowHeight - wallHeight);
}

public void drawInterface() {
  textFont(fontTitle, 160 / scale);
  fill(191, 208, 226);
  text("L.A.R.S.", 450 / scale, 143 / scale);
  
  textFont(fontGeneral, 54 / scale);
}

public void drawFilterSelectors() {
  for (int i = 0; i < amountFilters + 1; i++) {
    filterSelectors[i].draw();
  }
}

public void drawTracks() {
  for (int i = 0; i < amountTracks; i++) {
    tracks[i].draw();
  }
}

public void drawActors() {
  Iterator<Actor> iterator = actors.iterator();
  while (iterator.hasNext()) {
    iterator.next().draw();
  }
}

public void drawBarVisualizer() {
  barVisualizer.draw();
}



public void mouseClicked() {
  if (pcDebugging) {
    int lastControlled = pcDebuggingControl;
    pcDebuggingControl = actors.size();
    Actor newActor = new Actor(pcDebuggingControl, scale);
    newActor.syncTracking(actors.get(lastControlled).getPosX(), actors.get(lastControlled).getPosY());
    newActor.setFilter(actors.get(lastControlled).getFilter());
    actors.add(newActor);
  }
}

public void keyPressed() {
  if (key == '0') {
    pcDebuggingControl = 0;
  }
  if (key == '1') {
    pcDebuggingControl = 1;
  }
  if (key == '2') {
    pcDebuggingControl = 2;
  }
  if (key == '3') {
    pcDebuggingControl = 3;
  }
  if (key == '4') {
    pcDebuggingControl = 4;
  }
  if (key == '5') {
    pcDebuggingControl = 5;
  }
  if (key == '6') {
    pcDebuggingControl = 6;
  }
  if (key == '7') {
    pcDebuggingControl = 7;
  }
  if (key == '8') {
    pcDebuggingControl = 8;
  }
  if (key == '9') {
    pcDebuggingControl = 9;
  }
}
class Actor {
  protected int scale;
  
  protected int posX;
  protected int posY;
  
  protected Filter filter;
  
  protected int cursorId;
  
  
  public Actor(int cursorId, int scale) { 
    this.cursorId = cursorId;
    this.scale = scale;
  }
  
  
  public int getPosX() {
    return this.posX;
  }
  
  public int getPosY() {
    return this.posY;
  }
  
  public int getCursorId() {
    return this.cursorId;
  }
  
  public boolean hasFilter() {
    return this.filter != null;
  }
  
  public Filter getFilter() {
    return this.filter;
  }
  
  public void setFilter(Filter filter) {
    this.filter = filter;
  }
  
  public void clearFilter() {
    this.filter = null;
  }
  
  public void syncTracking(int posX, int posY) {
    this.posX = posX;
    this.posY = posY;
  }
  
  public void draw() {    
    // actor surrounding
    if (pcDebugging) {
      noStroke();
      fill(255, 255, 0, 64);
      ellipse(this.getPosX(), this.getPosY(), 50 / this.scale, 50 / this.scale);
      fill(0);
      text(this.getCursorId(), this.getPosX(), this.getPosY() - 20 / this.scale);
    }
    
    // filter
    if (this.hasFilter()) {
      this.filter.draw(this.posX, this.posY);
    }
  }
  
}
class BarVisualizer {
  
  protected int posX = 930 / scale; 
  protected int posYGroundLine = 255 / scale;
  protected int heightVis = 200 / scale;
  protected int barAmount = 40;
  protected int barWidth = 40 / scale;
  protected int barDist = 10 / scale;
  
  protected ArrayList<Track> tracks;
  
  
  public BarVisualizer() {
    this.tracks = new ArrayList<Track>();
  }
  
  public void addTrackReference(Track track) {
    this.tracks.add(track);
  }
  
  public void draw() {
    // calc amount running tracks and gather signal
    float[] totalSignal = new float[this.barAmount];
    int runningTracks = 0;
    float avgVolume = 0;
    
    Iterator<Track> iterator = this.tracks.iterator();
    while (iterator.hasNext()) {
      Track track = iterator.next();
      if (track.isPlaying()) {
        runningTracks++;
        avgVolume += track.getVolumeInPercent();
        
        int bufferPosBegin;
        for (int bar = 0; bar < this.barAmount; bar++) {
          bufferPosBegin = (int)map(bar, 0, this.barAmount, 0, track.getAudioOutput().bufferSize() - 1);
          for (int bufferPos = bufferPosBegin; bufferPos < bufferPosBegin + track.getAudioOutput().bufferSize() / this.barAmount; bufferPos++) {
            totalSignal[bar] += abs(track.getAudioOutput().left.get(bufferPos));
          }
        }
      }
    }
    
    // break down total
    float maxValue = 0;
    for (int bar = 0; bar < this.barAmount; bar++) {
      if (totalSignal[bar] > maxValue) {
        maxValue = totalSignal[bar];
      }
    }
    // mix volume
    avgVolume /= runningTracks;
    for (int bar = 0; bar < this.barAmount; bar++) {
      totalSignal[bar] /= maxValue;
      totalSignal[bar] *= avgVolume / 100;
    }
    
    // draw
    int xBar, size;
    fill(191, 208, 226);
    for (int bar = 0; bar < this.barAmount; bar++) {
      xBar = this.posX + (this.barWidth + this.barDist) * bar;
      size = (int)(totalSignal[bar] * heightVis);
      rect(xBar, this.posYGroundLine - size, this.barWidth, size);
    }
  }
}
class BitCrushEffect extends Filter {
  
  protected BitCrush filter;
  
  public BitCrushEffect(String name, float sampleRate, int scale) {
    super(name, sampleRate, scale);
    this.filterType = 4;
    this.filter = new BitCrush(16, 48000);
  }
  
  public UGen getFilterReference() {
    return this.filter;
  }
  
  public void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange) {
    float bitRes = map(localX, doubleMinRange / this.scale / 2f, doubleMaxRange / this.scale / 2f, 16f, 1f);
    if (bitRes < 1f) bitRes = 1f;
    if (bitRes > 16f) bitRes = 16f;
    this.filter.setBitRes(bitRes);
    this.filterParameters = "Resolution: " + (int)bitRes + "bit";
    this.showFilterParameters = true;
  }
  
  public void setFilterNoEffect() {
    this.filter.setBitRes(16f);
    this.showFilterParameters = false;
  }
}
class DelayEffect extends Filter {
  
  protected Delay filter;
  protected UGenSetter ugs;
  
  public DelayEffect(String name, float sampleRate, int scale) {
    super(name, sampleRate, scale);
    this.filterType = 2;
    this.filter = new Delay(.5f, 0f, true, true);
    this.ugs = new UGenSetter(this.filter);
  }
  
  public UGen getFilterReference() {
    return this.filter;
  }
  
  public void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange) {
    float delay = map(localX, doubleMinRange / this.scale / 2f, doubleMaxRange / this.scale / 2f, .05f, .5f);
    float amplitude = map(localY, doubleMaxRange / this.scale / 2f, -doubleMaxRange / this.scale / 2f, 0f, 1f);
    if (delay < .05f) delay = .05f;
    if (delay > .5f) delay = .5f;
    if (amplitude < 0) amplitude = 0;
    if (amplitude > 1) amplitude = 1;
    ugs.setDelayUGen(delay);
    ugs.setAmplitudeUGen(amplitude);
    this.filterParameters = "Feedback: " + (int)(amplitude * 100) + "%\nDelay: " + (int)(delay * 1000) + "ms";
    this.showFilterParameters = true;
  }
  
  public void setFilterNoEffect() {
    ugs.setDelayUGen(.5f);
    ugs.setAmplitudeUGen(0f);
    this.showFilterParameters = false;
  }
  
  public class UGenSetter extends UGen {
    protected Delay filter;
    
    public UGenSetter(Delay filter) {
      this.filter = filter;
    }
    public void uGenerate(float[] input) { }
    
    public void setDelayUGen(float value) {
      this.filter.delTime = new UGen.UGenInput(UGen.InputType.CONTROL, value);
    }
    
    public void setAmplitudeUGen(float value) {
      this.filter.delAmp = new UGen.UGenInput(UGen.InputType.CONTROL, value);
    }
  }
}


abstract class Filter {
  protected int scale;
  
  protected String name;
  
  protected float sampleRate;
  
  protected PImage icon;
  protected int iconSize = (int)(.4f * oneMeter);
  
  protected int iconAmount = 4;
  protected int iconAngleDist = (int)(.6f * oneMeter);
  protected float iconAngleVel = PI / 4;
  protected float iconAngle = 0f;
  
  protected boolean showFilterParameters = false;
  protected String filterParameters = "";
  
  protected int filterType = -1;
  
  public Filter(String name, float sampleRate, int scale) { 
    this.name = name;
    this.sampleRate = sampleRate;
    this.scale = scale;
  }
  
  public int getFilterType() {
    return this.filterType;
  }
  
  public String getFilterName() {
    return this.name;
  }
  
  public String getFilterParameters() {
    if (!showFilterParameters) return "";
    return filterParameters;
  }
  
  public void setIcon(String iconPath) {
    this.icon = loadImage(iconPath);
  }
  
  public void updateRotationIcon(float deltaTime) {
    this.iconAngle += iconAngleVel * deltaTime;
  }
  
  public abstract UGen getFilterReference();
  public abstract void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange);
  public abstract void setFilterNoEffect();
  
  public void draw(int x, int y) {
    tint(124, 197, 118);
    for (int i = 0; i < iconAmount; i++) {
      float angleOffsetX = iconAngleDist / this.scale * cos(iconAngle + i * 2 * PI / this.iconAmount);
      float angleOffsetY = iconAngleDist / this.scale * sin(iconAngle + i * 2 * PI / this.iconAmount);
      image(this.icon, x + angleOffsetX - (this.iconSize / 2) / this.scale, y + angleOffsetY - (this.iconSize / 2) / this.scale, this.iconSize / this.scale, this.iconSize / this.scale);
    }
  }
}
class FilterSelector {
  protected int scale;
  
  protected PImage icon;
  protected int posX, posYWall;
  protected int iconSize = (int)(1.3f * oneMeter);
 
  protected int startYWall;
  protected int activationSizeX = 2 * oneMeter;
  protected int activationSizeY = oneMeter;
  
  protected Filter filter;
  
  
  public FilterSelector(Filter filter, int startYWall, int scale) {
    this.filter = filter;
    this.startYWall = startYWall;
    this.scale = scale;
  }
  
  
  public void setIcon(String iconPath) {
    this.icon = loadImage(iconPath);
  }
  
  public void setPosition(int x, int yWall) {
    this.posX = x;
    this.posYWall = yWall;
  }
  
  public void checkAction(Actor actor) {
    if (actor.getPosX() >= this.posX - this.activationSizeX / 2 / this.scale && actor.getPosX() <= this.posX + this.activationSizeX / 2 / this.scale && actor.getPosY() <= this.startYWall + this.activationSizeY / this.scale) {
      actor.setFilter(this.filter);
    }
  }
  
  public void draw() {
    // draw instruction text
    fill(124, 197, 128);
    text("Schnapp dir einen Filter. Gehe zu einer aktivierten Spur. Modifiziere sie.", 1260 / this.scale, 1550 / this.scale);
    
    // draw icon
    if (this.filter != null) {
      tint(124, 197, 128);
    } else {
      tint(212, 97, 90);
    }
    image(this.icon, this.posX - (this.iconSize / 2) / this.scale, this.posYWall - (this.iconSize / 2) / this.scale, this.iconSize / this.scale, this.iconSize / this.scale);
    
    // draw selector (ground)
    noStroke();
    if (this.filter != null) {
      fill(124, 197, 128, 50);
    } else {
      fill(212, 97, 90, 50);
    }
    arc(this.posX, this.startYWall, this.activationSizeX / this.scale, this.activationSizeY * 2 / this.scale, 0, PI);
    
    // write filter-name (ground)
    noStroke();
    fill(0, 15, 30);
    if (this.filter != null) text(this.filter.getFilterName(), this.posX, this.startYWall + 70 / this.scale);
  }
}
class FlangerEffect extends Filter {
  
  protected ddf.minim.ugens.Flanger filter;
  protected UGenSetter ugs;
  
  public FlangerEffect(String name, float sampleRate, int scale) {
    super(name, sampleRate, scale);
    this.filterType = 3;
    this.filter = new ddf.minim.ugens.Flanger(1, .2f, 1, 0f, .5f, .0f);
    this.ugs = new UGenSetter(this.filter);
  }
  
  public UGen getFilterReference() {
    return this.filter;
  }
  
  public void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange) {
    float wet = map(localX, doubleMinRange / this.scale / 2f, doubleMaxRange / this.scale / 2f, 0f, 1f);
    float feedback = map(localY, doubleMaxRange / this.scale / 2f, -doubleMaxRange / this.scale / 2f, 0f, 1f);
    if (wet < 0f) wet = 0f;
    if (wet > 1f) wet = 1f;
    if (feedback < 0) feedback = 0;
    if (feedback > 1) feedback = 1;
    ugs.setWetUGen(wet);
    ugs.setFeedbackUGen(feedback);
    this.filterParameters = "Feedback: " + (int)(feedback * 100) + "%\nWet: " + (int)(wet * 100) + "%";
    this.showFilterParameters = true;
  }
  
  public void setFilterNoEffect() {
    ugs.setWetUGen(.0f);
    ugs.setFeedbackUGen(0f);
    this.showFilterParameters = false;
  }
  
  public class UGenSetter extends UGen {
    protected ddf.minim.ugens.Flanger filter;
    
    public UGenSetter(ddf.minim.ugens.Flanger filter) {
      this.filter = filter;
    }
    public void uGenerate(float[] input) { }
    
    public void setWetUGen(float value) {
      this.filter.wet = new UGen.UGenInput(UGen.InputType.CONTROL, value);
    }
    
    public void setFeedbackUGen(float value) {
      this.filter.feedback = new UGen.UGenInput(UGen.InputType.CONTROL, value);
    }
  }
}
class HighPassFilter extends Filter {
  
  protected HighPassSP filter;
  
  public HighPassFilter(String name, float sampleRate, int scale) {
    super(name, sampleRate, scale);
    this.filterType = 1;
    this.filter = new HighPassSP(1, this.sampleRate);
  }
  
  public UGen getFilterReference() {
    return this.filter;
  }
  
  public void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange) {
    float frequency = map(localX, doubleMinRange / this.scale / 2f, doubleMaxRange / this.scale / 2f, 200, 5000);
    if (frequency < 200) frequency = 200;
    if (frequency > 5000) frequency = 5000;
    this.filter.setFreq(frequency);
    this.filterParameters = "Cutoff: " + (int)frequency + " Hz";
    this.showFilterParameters = true;
  }
  
  public void setFilterNoEffect() {
    this.filter.setFreq(1);
    this.showFilterParameters = false;
  }
}
class LowPassFilter extends Filter {
  
  protected LowPassSP filter;
  
  public LowPassFilter(String name, float sampleRate, int scale) {
    super(name, sampleRate, scale);
    this.filterType = 0;
    this.filter = new LowPassSP(20000, this.sampleRate);
  }
  
  public UGen getFilterReference() {
    return this.filter;
  }
  
  public void setFilterValue(int localX, int localY, int doubleMinRange, int doubleMaxRange) {
    float frequency = map(localX, doubleMinRange / this.scale / 2f, doubleMaxRange / this.scale / 2f, 1000, 10);
    if (frequency < 10) frequency = 10;
    if (frequency > 1000) frequency = 1000;
    this.filter.setFreq(frequency);
    this.filterParameters = "Cutoff: " + (int)frequency + " Hz";
    this.showFilterParameters = true;
  }
  
  public void setFilterNoEffect() {
    this.filter.setFreq(20000);
    this.showFilterParameters = false;
  }
}

// Version 4.1




// these is just some helper code to make your code more readable
// for enhanced usage: http://www.tuio.org/?java 

TuioProcessing tuioClient;
boolean _DoDebug = false;
int _wallHeight = 0;
final int INVALID = -1;

public void initTracking(boolean doDebug, int wallHeight)
{
  _DoDebug = doDebug;
  _wallHeight = wallHeight;
  tuioClient  = new TuioProcessing(this);
}

public int GetNumTracks()
{
  return tuioClient.getTuioCursorList().size();
}

public int GetNumPathPoints(int trackID)
{
  if (trackID >= tuioClient.getTuioCursorList().size())
  {
    return 0;
  }
  TuioCursor tc =   tuioClient.getTuioCursorList().get(trackID);
  return tc.getPath().size();
}

public int GetPathPointX(int trackID, int pointID)
{
  if (trackID >= tuioClient.getTuioCursorList().size())
  {
    return INVALID;
  }
  TuioCursor tc = tuioClient.getTuioCursorList().get(trackID);
  ArrayList pointList = tc.getPath();
  if (pointList == null || pointList.size() <= pointID)
  {
    return INVALID;
  }

  TuioPoint tp = (TuioPoint)pointList.get(pointID);
  return tp.getScreenX(width);
}

public int GetPathPointY(int trackID, int pointID)
{
  if (trackID >= tuioClient.getTuioCursorList().size())
  {
    return INVALID;
  } 
  TuioCursor tc =   tuioClient.getTuioCursorList().get(trackID);  
  ArrayList pointList = tc.getPath();
  if (pointList == null || pointList.size() <=  pointID)
  {
    return INVALID;
  }

  TuioPoint tp = (TuioPoint)pointList.get(pointID);
  return tp.getScreenY(height - wallHeight)  + wallHeight;
}

public int GetX(int trackID)
{
  if (trackID >= tuioClient.getTuioCursorList().size())
  {
    return INVALID;
  } 
  TuioCursor tc =   tuioClient.getTuioCursorList().get(trackID); 
  return tc.getScreenX(width);
}
  
public int GetY(int trackID)
{
  if (trackID >= tuioClient.getTuioCursorList().size())
  {
    return INVALID;
  } 
  TuioCursor tc =   tuioClient.getTuioCursorList().get(trackID);   
  return tc.getScreenY(height - wallHeight)  + wallHeight;
}

public int GetCursorID(int trackID)
{
  if (trackID >= tuioClient.getTuioCursorList().size())
  {
    return INVALID;
  } 
  TuioCursor tc =   tuioClient.getTuioCursorList().get(trackID);   
  return tc.getCursorID();
}

public int GetNumFeet(int trackID)
{
  int nFeet = 0;
  ArrayList tuioObjectList = tuioClient.getTuioObjectList();  
  for (int i=0;i<tuioObjectList.size();i++) 
  {
    TuioObject tobj = (TuioObject)tuioObjectList.get(i);  
    if (tobj.getSymbolID() == trackID)
    {
       nFeet++;
    }
  }
  return nFeet; 
}

public int GetFootX(int trackID, int footID)
{
  int nFootCnt = 0;
  ArrayList tuioObjectList = tuioClient.getTuioObjectList();   
  for (int i=0;i<tuioObjectList.size();i++) 
  {
    TuioObject tobj = (TuioObject)tuioObjectList.get(i);  
    if (tobj.getSymbolID() == trackID)
    {
       if (nFootCnt == footID)
       {
         return tobj.getScreenX(width);
       }
       nFootCnt++;
    }
  }
  return INVALID;
}

public int GetFootY(int trackID, int footID)
{
  int nFootCnt = 0;
  ArrayList tuioObjectList = tuioClient.getTuioObjectList(); 
  for (int i=0;i<tuioObjectList.size();i++) 
  {
    TuioObject tobj = (TuioObject)tuioObjectList.get(i);  
    if (tobj.getSymbolID() == trackID)
    {
       if (nFootCnt == footID)
       {
         return tobj.getScreenY(height - wallHeight)  + wallHeight;
       }
       nFootCnt++;
    }
  }
  return INVALID;
}

// these callback methods are called whenever a TUIO event occurs

// called when an object is added to the scene
public void addTuioObject(TuioObject tobj) 
{
  if (_DoDebug)
    println("add object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle());
}

// called when an object is removed from the scene
public void removeTuioObject(TuioObject tobj) 
{
  if (_DoDebug)
    println("remove object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+")");
}

// called when an object is moved
public void updateTuioObject (TuioObject tobj) 
{
  if (_DoDebug)
    println("update object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle()+" "+tobj.getMotionSpeed()+" "+tobj.getRotationSpeed()+" "+tobj.getMotionAccel()+" "+tobj.getRotationAccel());
}

// called when a cursor is added to the scene
public void addTuioCursor(TuioCursor tcur) 
{
  if (_DoDebug)
    println("add cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") "+tcur.getX()+" "+tcur.getY());
}

// called when a cursor is moved
public void updateTuioCursor (TuioCursor tcur) 
{
  if (_DoDebug)
    println("update cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY()+" "+tcur.getMotionSpeed()+" "+tcur.getMotionAccel());
}

// called when a cursor is removed from the scene
public void removeTuioCursor(TuioCursor tcur) 
{
  if (_DoDebug)
    println("remove cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+")");
}

// called after each message bundle
// representing the end of an image frame
public void refresh(TuioTime bundleTime) 
{ 
  redraw();
}


public void addTuioBlob(TuioBlob tblb)
{
  if (_DoDebug)
    println("add blob "+tblb.getBlobID()+" ("+tblb.getSessionID()+ ") "+tblb.getX()+" "+tblb.getY());
}

public void removeTuioBlob(TuioBlob tblb)
{
  if (_DoDebug)
    println("remove blob "+tblb.getBlobID()+" ("+tblb.getSessionID()+ ") "+tblb.getX()+" "+tblb.getY());
}

public void updateTuioBlob(TuioBlob tblb)
{
  if (_DoDebug)
    println("update blob "+tblb.getBlobID()+" ("+tblb.getSessionID()+ ") "+tblb.getX()+" "+tblb.getY());
}





class Track {
  protected int scale;
  
  protected PImage icon;
  protected int posX, posY;
  protected int iconSize = (int)(.5f * oneMeter);
  protected boolean lowerRow = false;
  
  protected PImage muted;
  protected int mutedSize = (int)(.35f * oneMeter);
  
  protected int activationSize = oneMeter;
  protected int ripOffSize = 3 * oneMeter;
  
  protected int waveAmount = 3;
  protected int waveDist;
  protected float waveMaxAmpInPixels;
  protected float waveAngleVel = PI / 8;
  protected float waveAngle = 0f;
  
  protected AudioRecordingStream fileStream;
  protected FilePlayer audioPlayer;
  protected TickRate tickRate;
  protected float volumeInPercent = 100;
  protected Gain gain;
  protected float gainValue = 0f;
  protected AudioOutput out;
  protected boolean playing = false;
  protected Filter patchedFilter;
  
  protected ArrayList<Actor> nearActors;
  
  protected LowPassFilter lpf;
  protected HighPassFilter hpf;
  protected DelayEffect del;
  protected FlangerEffect fla;
  protected BitCrushEffect bit;
  
  
  public Track(Minim minim, String audioPath, int scale, boolean lowerRow) { 
    this.scale = scale;
    this.waveDist = this.activationSize / this.scale / 2 + (this.ripOffSize / this.scale / 2 - this.activationSize / this.scale / 2) / 16;
    this.waveMaxAmpInPixels = (this.activationSize / this.scale - waveDist) / 2;
    this.lowerRow = lowerRow;
    this.nearActors = new ArrayList<Actor>();
    this.fileStream = minim.loadFileStream(audioPath);
    this.tickRate = new TickRate(1.12f);
    this.tickRate.setInterpolation(true);
  }
  
  public void startStreaming() {
    this.audioPlayer = new FilePlayer(this.fileStream);
    this.audioPlayer.loop();
    
    this.lpf = new LowPassFilter("Tief-Pass", this.audioPlayer.sampleRate(), scale);
    this.lpf.setFilterNoEffect();
    UGen lpfE = this.lpf.getFilterReference();
    
    this.hpf = new HighPassFilter("Hoch-Pass", this.audioPlayer.sampleRate(), scale);
    this.hpf.setFilterNoEffect();
    UGen hpfE = this.hpf.getFilterReference();
    
    this.del = new DelayEffect("Delay", this.audioPlayer.sampleRate(), scale);
    this.del.setFilterNoEffect();
    UGen delE = this.del.getFilterReference();
    
    this.fla = new FlangerEffect("Flanger", this.audioPlayer.sampleRate(), scale);
    this.fla.setFilterNoEffect();
    UGen flaE = this.fla.getFilterReference();
    
    this.bit = new BitCrushEffect("BitCrush", this.audioPlayer.sampleRate(), scale);
    this.bit.setFilterNoEffect();
    UGen bitE = this.bit.getFilterReference();
    
    this.out = minim.getLineOut();
    this.gain = new Gain(this.gainValue);
    this.audioPlayer.patch(this.tickRate).patch(this.gain).patch(lpfE).patch(hpfE).patch(delE).patch(flaE).patch(bitE).patch(this.out);
    this.setVolumeInPercent(0f);
    this.mute();
  }
  
  
  public void setIcon(String iconPath) {
    this.icon = loadImage(iconPath);
  }
  
  public void setPosition(int x, int y) {
    this.posX = x;
    this.posY = y;
  }
  
  public void setVolumeInPercent(float volInPercent) {
    if (volInPercent > 95) volInPercent = 100;
    if (volInPercent < 5) volInPercent = 0;
    this.volumeInPercent = volInPercent;
    if (this.volumeInPercent == 0) {
      this.gainValue = -100;
    } else {
      this.gainValue = map(volInPercent, 100, 0, 0, -24);
    }
    this.gain.setValue(this.gainValue);
  }
  
  public AudioOutput getAudioOutput() {
    return this.out;
  }
  
  public void play() {
    this.out.unmute();
    this.playing = true;
  }
  
  public void muteWithoutDeactivating() {
    this.out.mute();
  }
  
  public void mute() {
    this.out.mute();
    this.playing = false;
  }
  
  public boolean isPlaying() {
    return this.playing;
  }
  
  public float getVolumeInPercent() {
    return this.volumeInPercent;
  }
  
  public float getSampleRate() {
    return this.audioPlayer.sampleRate();
  }
  
  public void checkAction(Actor actor) {
    int localPosX = actor.getPosX() - this.posX;
    int localPosY = actor.getPosY() - this.posY;
    
    if (!this.isPlaying()) {
      // is not playing
      if (localPosX * localPosX + localPosY * localPosY < (this.activationSize / 2) * (this.activationSize / 2) / this.scale / this.scale) {
        // someone inside activation
        if (!actor.hasFilter()) {
          // box or no-filter actor -> play track and save as activator
          this.nearActors.add(0, actor);
          this.setVolumeInPercent(50f);
          this.play();
        }
      }
    } else {
      // is playing
      if (localPosX * localPosX + localPosY * localPosY < (this.activationSize / 2) * (this.activationSize / 2) / this.scale / this.scale) {
        // someone inside activation
        this.addNewActor(actor);
      } else if (localPosX * localPosX + localPosY * localPosY > (this.activationSize / 2) * (this.activationSize / 2) / this.scale / this.scale && localPosX > 0 && !actor.hasFilter()) {
          // actor without filter outside right
          this.removeActor(actor);
      } else if (localPosX * localPosX + localPosY * localPosY > (this.activationSize / 2) * (this.activationSize / 2) / this.scale / this.scale && localPosX < 0 && actor.hasFilter()) {
          // actor with filter outside left
          this.removeActor(actor);
      } else if (localPosX * localPosX + localPosY * localPosY > (this.ripOffSize / 2) * (this.ripOffSize / 2) / this.scale / this.scale) {
          // actor outside rip-off
          this.removeActor(actor);
      }

      if (this.nearActors.isEmpty()) {
        // no more actors/activating actor is gone
        this.setVolumeInPercent(0f);
        this.mute();
      }
    }
  }
  
  public void updateVolume() {
    Iterator<Actor> iteratorActors = this.nearActors.iterator();
      while (iteratorActors.hasNext()) {
        Actor actor = iteratorActors.next();
        if (!actor.hasFilter()) {
          // no filter -> check
          int localPosX = actor.getPosX() - this.posX;
          int localPosY = actor.getPosY() - this.posY;
          if (localPosX < 0 && localPosX * localPosX + localPosY * localPosY > (this.activationSize / 2) * (this.activationSize / 2) / this.scale / this.scale) {
            // on volume selector
            if (localPosY == 0) {
              this.setVolumeInPercent(66.666666f);
            } else if (localPosX == 0) {
              this.setVolumeInPercent(0);
            } else {
              // high = 90 - 135 deg
              // mid = 135 - 225 deg
              // low = 225 - 270 deg
              float mathX = localPosX;
              float mathY = -localPosY;
              if (abs(mathY) > abs(mathX) && mathY > 0) {
                // high
                this.setVolumeInPercent(0);
              } else if (abs(mathY) > abs(mathX) && mathY < 0) {
                // low
                float angle = atan(mathX / mathY) * 180 / PI;
                this.setVolumeInPercent(map(angle, 45, 10, 30.43479f, 0));
              } else {
                // mid
                float angle = atan(mathY / mathX) * 180 / PI;
                this.setVolumeInPercent(map(angle, -35, 45, 100, 30.43479f));
              }
            }
            return;
          }
        }
      }
  }
  
  public void updateFilters() {
    if (!this.hasNearActorWithFilter() && this.patchedFilter != null) {
      // unpatch
      this.lpf.setFilterNoEffect();
      this.hpf.setFilterNoEffect();
      this.del.setFilterNoEffect();
      this.fla.setFilterNoEffect();
      this.bit.setFilterNoEffect();
      this.patchedFilter = null;
    } else if (this.hasNearActorWithFilter() && this.patchedFilter == null) {
      // patch filter
      Iterator<Actor> iteratorActors = this.nearActors.iterator();
      while (iteratorActors.hasNext()) {
        Actor actor = iteratorActors.next();
        if (actor.hasFilter()) {
          Filter newFilter = actor.getFilter();
          if (newFilter.getFilterType() == 0) {
            this.lpf.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
            this.patchedFilter = this.lpf;
          } else if (newFilter.getFilterType() == 1) {
            this.hpf.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
            this.patchedFilter = this.hpf;
          } else if (newFilter.getFilterType() == 2) {
            this.del.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
            this.patchedFilter = this.del;
          } else if (newFilter.getFilterType() == 3) {
            this.fla.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
            this.patchedFilter = this.fla;
          } else if (newFilter.getFilterType() == 4) {
            this.bit.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
            this.patchedFilter = this.bit;
          }
          return;
        }
      }
    } else if (this.hasNearActorWithFilter() && this.patchedFilter != null) {
      // change existing filter
      Iterator<Actor> iteratorActors = this.nearActors.iterator();
      while (iteratorActors.hasNext()) {
        Actor actor = iteratorActors.next();
        if (actor.hasFilter()) {
          Filter newFilter = actor.getFilter();
          if (newFilter.getFilterType() == 0 && newFilter.getFilterType() == this.patchedFilter.getFilterType()) {
            this.lpf.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
          } else if (newFilter.getFilterType() == 1 && newFilter.getFilterType() == this.patchedFilter.getFilterType()) {
            this.hpf.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
          } else if (newFilter.getFilterType() == 2) {
            this.del.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
          } else if (newFilter.getFilterType() == 3) {
            this.fla.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
          } else if (newFilter.getFilterType() == 4) {
            this.bit.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
          }
          return;
        }
      }
    }
  }
  
  protected void addNewActor(Actor actor) {
    if (this.nearActors.size() >= 1 && !actor.hasFilter()) return; // if actor has no filter then don't add except activator
    if (this.getActor(actor.getCursorId()) == null) {
      this.nearActors.add(actor);
    }
  }
  
  protected Actor getActor(int cursorId) {
    Iterator<Actor> iterator = nearActors.iterator();
    while (iterator.hasNext()) {
      Actor actor = iterator.next();
      if (actor.getCursorId() == cursorId) {
        return actor;
      }
    }
    return null;
  }
  
  protected void removeActor(Actor actor) {
    this.nearActors.remove(getActor(actor.getCursorId()));
  }
  
  protected boolean hasNearActorWithFilter() {
    Iterator<Actor> iterator = nearActors.iterator();
    while (iterator.hasNext()) {
      Actor actor = iterator.next();
      if (actor.hasFilter()) {
        return true;
      }
    }
    return false;
  }
  
  public void draw() { 
    // draw volume selector
    if (this.isPlaying()) {
      // outer circle
      float maxSize = this.ripOffSize / this.scale / 2;
      float minSize = this.activationSize / this.scale / 2 + (this.ripOffSize / this.scale / 2 - this.activationSize / this.scale / 2) / 3;
      float startMax = HALF_PI * 1.5f;
      float endMin = 3 * HALF_PI;
      float angleDegStep = 1f;
      noFill();
      strokeWeight(5);
      float angle = HALF_PI;
      float angleEnd = 3 * HALF_PI;
      float sizeRadius = minSize;
      float colorStartMaxR = 164;
      float colorStartMaxG = 31;
      float colorStartMaxB = 193;
      float colorEndMinR = 16;
      float colorEndMinG = 3;
      float colorEndMinB = 19;
      float colorR = colorEndMinR;
      float colorG = colorEndMinG;
      float colorB = colorEndMinB;
      float xInner;
      float xOuter;
      float yInner;
      float yOuter;
      while (angle <= angleEnd) {
        if (angle < startMax) {
          // muted area
          sizeRadius = this.activationSize / this.scale / 2 + (this.ripOffSize / this.scale / 2 - this.activationSize / this.scale / 2) / 16 * 7;
        } else {
          sizeRadius = map(angle, startMax, endMin, maxSize, minSize);
          colorR = map(angle, startMax, endMin, colorStartMaxR, colorEndMinR);
          colorG = map(angle, startMax, endMin, colorStartMaxG, colorEndMinG);
          colorB = map(angle, startMax, endMin, colorStartMaxB, colorEndMinB);
        }
        xInner = this.posX + minSize * sin(angle + HALF_PI);
        yInner = this.posY + minSize * cos(angle + HALF_PI);
        xOuter = this.posX + sizeRadius * sin(angle + HALF_PI);
        yOuter = this.posY + sizeRadius * cos(angle + HALF_PI);
        stroke(colorR, colorG, colorB);
        line(xInner, yInner, xOuter, yOuter);
        angle += angleDegStep / 180 * PI;
      }
      
      // draw volume selector - inner circle
      strokeWeight(2);
      stroke(164, 31, 193);
      arc(this.posX, this.posY, minSize * 2, minSize * 2, HALF_PI, 3 * HALF_PI);
      
      // muted sign
      noStroke();
      tint(164, 31, 193);
      if (this.muted == null) {
        this.muted = loadImage("muted.png");
      }
      image(this.muted, this.posX + (minSize + maxSize) / 2 * sin(2.25f * HALF_PI) - this.mutedSize / 2 / this.scale, this.posY + (minSize + maxSize) / 2 * cos(2.25f * HALF_PI) - this.mutedSize / 2 / this.scale, this.mutedSize / this.scale, this.mutedSize / this.scale);
      
      // draw rip-off for effects
      if (this.hasNearActorWithFilter()) {
        strokeWeight(2);
        stroke(124, 197, 118);
        noFill();
        line(this.posX, this.posY + minSize, this.posX, this.posY + this.ripOffSize / this.scale / 2);
        line(this.posX, this.posY - minSize, this.posX, this.posY - this.ripOffSize / this.scale / 2);
        // outer circle
        arc(this.posX, this.posY, this.ripOffSize / this.scale, this.ripOffSize / this.scale, -HALF_PI, HALF_PI);
        // inner circle
        arc(this.posX, this.posY, minSize * 2, minSize * 2, -HALF_PI, HALF_PI);
      }
    }
    
    // draw activation circle
    noStroke();
    fill(0, 51, 102, 90);
    ellipse(this.posX, this.posY, this.activationSize / this.scale, this.activationSize / this.scale);
    
    // draw wave
    drawWave();    
    
    // draw active status on wall
    if (this.isPlaying()) {
      noStroke();
      fill(164, 31, 193);
    } else {
      noStroke();
      fill(190, 165, 217);
    }
    float posDifferenceY = 2000 / this.scale;
    float posDifferenceX = 80f / this.scale;
    if (this.lowerRow) {
      posDifferenceY = 2200 / this.scale;
      posDifferenceX = 360 / this.scale;
    }
    ellipse(this.posX + posDifferenceX - 150 / this.scale, this.posY - posDifferenceY, this.activationSize / this.scale * 1.5f, this.activationSize / this.scale * 1.5f);
    noStroke();
    fill(0, 26, 51);
    ellipse(this.posX + posDifferenceX - 150 / this.scale, this.posY - posDifferenceY, this.activationSize / this.scale * 4f / 5f * 1.5f, this.activationSize / this.scale * 4f / 5f * 1.5f);
    fill(0, 0, 0);
    
    // draw information about the track on wall
    fill(191, 208, 226);
    String informationText = "";
    if (this.playing) {
      informationText += "Aktiv\n";
      informationText += "Volume: " + (int)this.volumeInPercent + "%\n";
    } else {
      informationText += "Inaktiv\n";
    }
    String filterName = "";
    Iterator<Actor> iterator = nearActors.iterator();
    while (iterator.hasNext()) {
      Actor actor = iterator.next();
      if (actor.hasFilter()) {
        filterName = "Filter: " + actor.getFilter().getFilterName() + "\n";      
        if (actor.getFilter().getFilterType() == 0) {
          filterName += this.lpf.getFilterParameters();
        } else if (actor.getFilter().getFilterType() == 1) {
          filterName += this.hpf.getFilterParameters();
        } else if (actor.getFilter().getFilterType() == 2) {
          filterName += this.del.getFilterParameters();
        } else if (actor.getFilter().getFilterType() == 3) {
          filterName += this.fla.getFilterParameters();
        } else if (actor.getFilter().getFilterType() == 4) {
          filterName += this.bit.getFilterParameters();
        }
      }
    }
    informationText += filterName;
    textFont(fontGeneral, 48 / this.scale);
    text(informationText, this.posX + posDifferenceX - 150 / this.scale, this.posY - posDifferenceY + 180 / this.scale);
    
    // draw icon on ground and wall
    if (!this.isPlaying()) {
      tint(190, 165, 217);
    } else {
      tint(164, 31, 193);
    }
    image(this.icon, this.posX - (this.iconSize / 2) / this.scale, this.posY - (this.iconSize / 2) / this.scale, this.iconSize / this.scale, this.iconSize / this.scale);
    image(this.icon, this.posX + posDifferenceX - 150 / this.scale - (this.iconSize / 2) / this.scale, this.posY - (this.iconSize / 2) / this.scale - posDifferenceY, this.iconSize / this.scale, this.iconSize / this.scale);
  }
  
  public void updateRotationWave(float deltaTime) {
    this.waveAngle += waveAngleVel * deltaTime;
  }
  
  protected void drawWave() {
    if (this.isPlaying()) {
      stroke(164, 31, 193);
    } else {
      stroke(190, 165, 217);
    }
    strokeWeight(1);
    float waveAngleDistance = 2 * PI / this.waveAmount;
    float waveAngleWidth = waveAngleDistance / 3 * 2;
    float waveAngleSteps = 4f * PI / 180;
    float tempWaveAngle = this.waveAngle;
    float x1;
    float x2 = -1;
    float y1;
    float y2 = -1;
    while (tempWaveAngle <= waveAngleWidth + this.waveAngle - waveAngleSteps) {
      for (int waveId = 0; waveId < this.waveAmount; waveId++) {
        int bufferPos1 = (int)map(tempWaveAngle, this.waveAngle, this.waveAngle + waveAngleWidth, 0, this.out.bufferSize());
        if (bufferPos1 < 0) bufferPos1 = 0;
        if (bufferPos1 >= this.out.bufferSize()) bufferPos1 = this.out.bufferSize() - 1;
        float amp1 = this.out.left.get(bufferPos1) * 1.5f;
        if (amp1 < 0) amp1 = 0;
        if (amp1 > 1) amp1 = 1;
        int bufferPos2 = (int)map(tempWaveAngle + waveAngleSteps, this.waveAngle, this.waveAngle + waveAngleWidth, 0, this.out.bufferSize());
        if (bufferPos2 < 0) bufferPos2 = 0;
        if (bufferPos2 >= this.out.bufferSize()) bufferPos2 = this.out.bufferSize() - 1;
        float amp2 = this.out.left.get(bufferPos2) * 1.5f;
        if (amp2 < 0) amp2 = 0;
        if (amp2 > 1) amp2 = 1;
        x1 = this.posX + (this.waveDist + waveMaxAmpInPixels * amp1) * sin(tempWaveAngle + waveId * waveAngleDistance);
        y1 = this.posY + (this.waveDist + waveMaxAmpInPixels * amp1) * cos(tempWaveAngle + waveId * waveAngleDistance);
        x2 = this.posX + (this.waveDist + waveMaxAmpInPixels * amp2) * sin(tempWaveAngle + waveId * waveAngleDistance);
        y2 = this.posY + (this.waveDist + waveMaxAmpInPixels * amp2) * cos(tempWaveAngle + waveId * waveAngleDistance);
        if (x1 != -1) {
          line(x1, y1, x2, y2);
        }
      }
      tempWaveAngle += waveAngleSteps;
    }
  }
  
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "LARS" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
