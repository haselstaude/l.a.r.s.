class Actor {
  protected int scale;
  
  protected int posX;
  protected int posY;
  
  protected Filter filter;
  
  protected int cursorId;
  
  
  public Actor(int cursorId, int scale) { 
    this.cursorId = cursorId;
    this.scale = scale;
  }
  
  
  public int getPosX() {
    return this.posX;
  }
  
  public int getPosY() {
    return this.posY;
  }
  
  public int getCursorId() {
    return this.cursorId;
  }
  
  public boolean hasFilter() {
    return this.filter != null;
  }
  
  public Filter getFilter() {
    return this.filter;
  }
  
  public void setFilter(Filter filter) {
    this.filter = filter;
  }
  
  public void clearFilter() {
    this.filter = null;
  }
  
  public void syncTracking(int posX, int posY) {
    this.posX = posX;
    this.posY = posY;
  }
  
  public void draw() {    
    // actor surrounding
    if (pcDebugging) {
      noStroke();
      fill(255, 255, 0, 64);
      ellipse(this.getPosX(), this.getPosY(), 50 / this.scale, 50 / this.scale);
      fill(0);
      text(this.getCursorId(), this.getPosX(), this.getPosY() - 20 / this.scale);
    }
    
    // filter
    if (this.hasFilter()) {
      this.filter.draw(this.posX, this.posY);
    }
  }
  
}