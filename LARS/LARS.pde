import ddf.minim.*;


// tracking
boolean pcDebugging = false;    // true: mouse used for tracking and scaled output, false: TUIO-tracking and fullscreen output
int pcDebuggingControl = 0;
ArrayList<Actor> actors;
int fpsTracking = 20;
int timeLastTrack;

// audio
Minim minim;
Track[] tracks;
int amountTracks = 8;
Filter[] filters;
FilterSelector[] filterSelectors;
int amountFilters = 5;

// rendering
PFont fontTitle;
PFont fontGeneral;
int scale = 4;
int windowWidth = 3030 / scale;
int windowHeight = 3712 / scale;
int wallHeight = 1914 / scale;
int oneMeter = (3712 - 1914) / 9;
BarVisualizer barVisualizer;

void settings() {
  size(windowWidth, windowHeight);
  if (!pcDebugging) fullScreen();
}



void setup() {
  // fonts
  fontTitle = loadFont("ShareTechMono.vlw");
  fontGeneral = loadFont("Consolas.vlw");
  textAlign(CENTER, TOP);
  
  // actors
  actors = new ArrayList<Actor>();
  if (pcDebugging) actors.add(new Actor(pcDebuggingControl, scale));
  
  // elements
  minim = new Minim(this);
  tracks = new Track[amountTracks];
  filters = new Filter[amountFilters];
  filterSelectors = new FilterSelector[amountFilters + 1];
  setupTracks();
  delay(1000);
  startTracks();
  setupFilter();
  setupFilterSelection();
  
  // bar-visualizer
  barVisualizer = new BarVisualizer();
  for (int i = 0; i < amountTracks; i++) {
    barVisualizer.addTrackReference(tracks[i]);
  }
  
  // tracking
  if (!pcDebugging) initTracking(false, wallHeight);
}



void setupTracks() {
  tracks[0] = new Track(minim, "tracks/audio/Electro_Drum.wav", scale, false);
  tracks[0].setIcon("tracks/icon/Electro_Drum.png");
  tracks[0].setPosition(366 / scale, wallHeight + 600 / scale);
  
  tracks[1] = new Track(minim, "tracks/audio/Noise.wav", scale, false);
  tracks[1].setIcon("tracks/icon/Noise.png");
  tracks[1].setPosition(1066 / scale, wallHeight + 600 / scale);
  
  tracks[2] = new Track(minim, "tracks/audio/Melodic.wav", scale, false);
  tracks[2].setIcon("tracks/icon/Melodic.png");
  tracks[2].setPosition(1766 / scale, wallHeight + 600 / scale);
  
  tracks[3] = new Track(minim, "tracks/audio/Slow_Drum.wav", scale, false);
  tracks[3].setIcon("tracks/icon/Slow_Drum.png");
  tracks[3].setPosition(2466 / scale, wallHeight + 600 / scale);
  
  tracks[4] = new Track(minim, "tracks/audio/Trance.wav", scale, true);
  tracks[4].setIcon("tracks/icon/Trance.png");
  tracks[4].setPosition(432 / scale, wallHeight + 1350 / scale);
  
  tracks[5] = new Track(minim, "tracks/audio/Octave.wav", scale, true);
  tracks[5].setIcon("tracks/icon/Octave.png");
  tracks[5].setPosition(1132 / scale, wallHeight + 1350 / scale);
  
  tracks[6] = new Track(minim, "tracks/audio/Fast_Drum.wav", scale, true);
  tracks[6].setIcon("tracks/icon/Fast_Drum.png");
  tracks[6].setPosition(1832 / scale, wallHeight + 1350 / scale);
  
  tracks[7] = new Track(minim, "tracks/audio/Guitar.wav", scale, true);
  tracks[7].setIcon("tracks/icon/Guitar.png");
  tracks[7].setPosition(2532 / scale, wallHeight + 1350 / scale);
}

void setupFilter() {  
  filters[0] = new LowPassFilter("Tief-Pass", tracks[0].getSampleRate(), scale);
  filters[0].setIcon("filters/icon/lowpass.png");
  
  filters[1] = new HighPassFilter("Hoch-Pass", tracks[0].getSampleRate(), scale);
  filters[1].setIcon("filters/icon/highpass.png");
  
  filters[2] = new DelayEffect("Delay", tracks[0].getSampleRate(), scale);
  filters[2].setIcon("filters/icon/reverb.png");
  
  filters[3] = new FlangerEffect("Flanger", tracks[0].getSampleRate(), scale);
  filters[3].setIcon("filters/icon/flanger.png");
  
  filters[4] = new BitCrushEffect("BitCrush", tracks[0].getSampleRate(), scale);
  filters[4].setIcon("filters/icon/distortion.png");
}

void setupFilterSelection() {
  filterSelectors[0] = new FilterSelector(filters[0], wallHeight, scale);
  filterSelectors[0].setIcon("filters/icon/lowpass.png");
  filterSelectors[0].setPosition(250 / scale, 1750 / scale);
  
  filterSelectors[1] = new FilterSelector(filters[1], wallHeight, scale);
  filterSelectors[1].setIcon("filters/icon/highpass.png");
  filterSelectors[1].setPosition(750 / scale, 1750 / scale);
  
  filterSelectors[2] = new FilterSelector(filters[2], wallHeight, scale);
  filterSelectors[2].setIcon("filters/icon/reverb.png");
  filterSelectors[2].setPosition(1250 / scale, 1750 / scale);
  
  filterSelectors[3] = new FilterSelector(filters[3], wallHeight, scale);
  filterSelectors[3].setIcon("filters/icon/flanger.png");
  filterSelectors[3].setPosition(1750 / scale, 1750 / scale);
  
  filterSelectors[4] = new FilterSelector(filters[4], wallHeight, scale);
  filterSelectors[4].setIcon("filters/icon/distortion.png");
  filterSelectors[4].setPosition(2250 / scale, 1750 / scale);
  
  filterSelectors[5] = new FilterSelector(null, wallHeight, scale);
  filterSelectors[5].setIcon("filters/icon/nofilter.png");
  filterSelectors[5].setPosition(2750 / scale, 1750 / scale);
}

void startTracks() {
  for (int i = 0; i < amountTracks; i++) {
    this.tracks[i].startStreaming();
  }
}



void draw() {
  int updateStart = millis();
  // update
  if (millis() - timeLastTrack >= 1000 / fpsTracking) {
    updateTrackingData();
    timeLastTrack = millis();
  }
  updateFilters();
  updateTracks();
  int updateEndDrawStart = millis();
  int updateDuration = updateEndDrawStart - updateStart;
  
  // draw
  drawRoom();
  drawInterface();
  drawFilterSelectors();
  drawTracks();
  drawActors();
  drawBarVisualizer();
  int drawDuration = millis() - updateEndDrawStart;
  
  // FPS
  surface.setTitle("L.A.R.S. | " + (int)frameRate + " FPS | Update: " + updateDuration + "ms | Draw: " + drawDuration + "ms");
  textFont(fontGeneral, 36 / scale);
  textAlign(RIGHT, TOP);
  text((int)frameRate + " FPS | Update: " + updateDuration + "ms | Draw: " + drawDuration + "ms", windowWidth - 50 / scale, 50 / scale); 
  textAlign(CENTER, TOP);
  textFont(fontGeneral, 54 / scale);
}



void updateTrackingData() {
  if (pcDebugging) {
    actors.get(pcDebuggingControl).syncTracking(mouseX, mouseY);
  } else {
    ArrayList<Actor> newActorList = new ArrayList<Actor>();
    
    // iterate through tracking-tracks
    for (int trackId = 0; trackId < GetNumTracks(); trackId++) {
      Actor knownActor = getActor(GetCursorID(trackId));
      Actor newActor = new Actor(GetCursorID(trackId), scale);
      if (knownActor != null) {
        // known actor -- copy filter
        newActor.setFilter(knownActor.getFilter());
      }
      newActor.syncTracking(GetX(trackId), GetY(trackId));
      newActorList.add(newActor);
    }
    
    actors = newActorList;
  }
}

Actor getActor(int cursorId) {
  Iterator<Actor> iterator = actors.iterator();
  while (iterator.hasNext()) {
    Actor actor = iterator.next();
    if (actor.getCursorId() == cursorId) {
      return actor;
    }
  }
  return null;
}



void updateFilters() {
  // filter-selectors
  for (int i = 0; i < amountFilters + 1; i++) {
    if (i < amountFilters) 
      filters[i].updateRotationIcon(1f / frameRate);
      
    Iterator<Actor> iterator = actors.iterator();
    while (iterator.hasNext()) {
      filterSelectors[i].checkAction(iterator.next());
    }
  }
}

void updateTracks() {
  for (int i = 0; i < amountTracks; i++) {
    tracks[i].updateFilters();
    tracks[i].updateVolume();
    Iterator<Actor> iterator = actors.iterator();
    while (iterator.hasNext()) {
      tracks[i].checkAction(iterator.next());
    }
    tracks[i].updateRotationWave(1f / frameRate);
  }
}



void drawRoom() {
  background(255);
  
  // wall
  noStroke();
  fill(0, 15, 30);
  rect(0, 0, windowWidth, wallHeight);
  
  // floor
  noStroke();
  fill(0, 15, 30);
  rect(0, wallHeight, windowWidth, windowHeight - wallHeight);
}

void drawInterface() {
  textFont(fontTitle, 160 / scale);
  fill(191, 208, 226);
  text("L.A.R.S.", 450 / scale, 143 / scale);
  
  textFont(fontGeneral, 54 / scale);
}

void drawFilterSelectors() {
  for (int i = 0; i < amountFilters + 1; i++) {
    filterSelectors[i].draw();
  }
}

void drawTracks() {
  for (int i = 0; i < amountTracks; i++) {
    tracks[i].draw();
  }
}

void drawActors() {
  Iterator<Actor> iterator = actors.iterator();
  while (iterator.hasNext()) {
    iterator.next().draw();
  }
}

void drawBarVisualizer() {
  barVisualizer.draw();
}



void mouseClicked() {
  if (pcDebugging) {
    int lastControlled = pcDebuggingControl;
    pcDebuggingControl = actors.size();
    Actor newActor = new Actor(pcDebuggingControl, scale);
    newActor.syncTracking(actors.get(lastControlled).getPosX(), actors.get(lastControlled).getPosY());
    newActor.setFilter(actors.get(lastControlled).getFilter());
    actors.add(newActor);
  }
}

void keyPressed() {
  if (key == '0') {
    pcDebuggingControl = 0;
  }
  if (key == '1') {
    pcDebuggingControl = 1;
  }
  if (key == '2') {
    pcDebuggingControl = 2;
  }
  if (key == '3') {
    pcDebuggingControl = 3;
  }
  if (key == '4') {
    pcDebuggingControl = 4;
  }
  if (key == '5') {
    pcDebuggingControl = 5;
  }
  if (key == '6') {
    pcDebuggingControl = 6;
  }
  if (key == '7') {
    pcDebuggingControl = 7;
  }
  if (key == '8') {
    pcDebuggingControl = 8;
  }
  if (key == '9') {
    pcDebuggingControl = 9;
  }
}