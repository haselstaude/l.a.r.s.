import ddf.minim.*;
import ddf.minim.ugens.*;
import ddf.minim.spi.*;
import java.util.Iterator;

class Track {
  protected int scale;
  
  protected PImage icon;
  protected int posX, posY;
  protected int iconSize = (int)(.5 * oneMeter);
  protected boolean lowerRow = false;
  
  protected PImage muted;
  protected int mutedSize = (int)(.35 * oneMeter);
  
  protected int activationSize = oneMeter;
  protected int ripOffSize = 3 * oneMeter;
  
  protected int waveAmount = 3;
  protected int waveDist;
  protected float waveMaxAmpInPixels;
  protected float waveAngleVel = PI / 8;
  protected float waveAngle = 0f;
  
  protected AudioRecordingStream fileStream;
  protected FilePlayer audioPlayer;
  protected TickRate tickRate;
  protected float volumeInPercent = 100;
  protected Gain gain;
  protected float gainValue = 0f;
  protected AudioOutput out;
  protected boolean playing = false;
  protected Filter patchedFilter;
  
  protected ArrayList<Actor> nearActors;
  
  protected LowPassFilter lpf;
  protected HighPassFilter hpf;
  protected DelayEffect del;
  protected FlangerEffect fla;
  protected BitCrushEffect bit;
  
  
  public Track(Minim minim, String audioPath, int scale, boolean lowerRow) { 
    this.scale = scale;
    this.waveDist = this.activationSize / this.scale / 2 + (this.ripOffSize / this.scale / 2 - this.activationSize / this.scale / 2) / 16;
    this.waveMaxAmpInPixels = (this.activationSize / this.scale - waveDist) / 2;
    this.lowerRow = lowerRow;
    this.nearActors = new ArrayList<Actor>();
    this.fileStream = minim.loadFileStream(audioPath);
    this.tickRate = new TickRate(1.12f);
    this.tickRate.setInterpolation(true);
  }
  
  public void startStreaming() {
    this.audioPlayer = new FilePlayer(this.fileStream);
    this.audioPlayer.loop();
    
    this.lpf = new LowPassFilter("Tief-Pass", this.audioPlayer.sampleRate(), scale);
    this.lpf.setFilterNoEffect();
    UGen lpfE = this.lpf.getFilterReference();
    
    this.hpf = new HighPassFilter("Hoch-Pass", this.audioPlayer.sampleRate(), scale);
    this.hpf.setFilterNoEffect();
    UGen hpfE = this.hpf.getFilterReference();
    
    this.del = new DelayEffect("Delay", this.audioPlayer.sampleRate(), scale);
    this.del.setFilterNoEffect();
    UGen delE = this.del.getFilterReference();
    
    this.fla = new FlangerEffect("Flanger", this.audioPlayer.sampleRate(), scale);
    this.fla.setFilterNoEffect();
    UGen flaE = this.fla.getFilterReference();
    
    this.bit = new BitCrushEffect("BitCrush", this.audioPlayer.sampleRate(), scale);
    this.bit.setFilterNoEffect();
    UGen bitE = this.bit.getFilterReference();
    
    this.out = minim.getLineOut();
    this.gain = new Gain(this.gainValue);
    this.audioPlayer.patch(this.tickRate).patch(this.gain).patch(lpfE).patch(hpfE).patch(delE).patch(flaE).patch(bitE).patch(this.out);
    this.setVolumeInPercent(0f);
    this.mute();
  }
  
  
  public void setIcon(String iconPath) {
    this.icon = loadImage(iconPath);
  }
  
  public void setPosition(int x, int y) {
    this.posX = x;
    this.posY = y;
  }
  
  public void setVolumeInPercent(float volInPercent) {
    if (volInPercent > 95) volInPercent = 100;
    if (volInPercent < 5) volInPercent = 0;
    this.volumeInPercent = volInPercent;
    if (this.volumeInPercent == 0) {
      this.gainValue = -100;
    } else {
      this.gainValue = map(volInPercent, 100, 0, 0, -24);
    }
    this.gain.setValue(this.gainValue);
  }
  
  public AudioOutput getAudioOutput() {
    return this.out;
  }
  
  public void play() {
    this.out.unmute();
    this.playing = true;
  }
  
  public void muteWithoutDeactivating() {
    this.out.mute();
  }
  
  public void mute() {
    this.out.mute();
    this.playing = false;
  }
  
  public boolean isPlaying() {
    return this.playing;
  }
  
  public float getVolumeInPercent() {
    return this.volumeInPercent;
  }
  
  public float getSampleRate() {
    return this.audioPlayer.sampleRate();
  }
  
  public void checkAction(Actor actor) {
    int localPosX = actor.getPosX() - this.posX;
    int localPosY = actor.getPosY() - this.posY;
    
    if (!this.isPlaying()) {
      // is not playing
      if (localPosX * localPosX + localPosY * localPosY < (this.activationSize / 2) * (this.activationSize / 2) / this.scale / this.scale) {
        // someone inside activation
        if (!actor.hasFilter()) {
          // box or no-filter actor -> play track and save as activator
          this.nearActors.add(0, actor);
          this.setVolumeInPercent(50f);
          this.play();
        }
      }
    } else {
      // is playing
      if (localPosX * localPosX + localPosY * localPosY < (this.activationSize / 2) * (this.activationSize / 2) / this.scale / this.scale) {
        // someone inside activation
        this.addNewActor(actor);
      } else if (localPosX * localPosX + localPosY * localPosY > (this.activationSize / 2) * (this.activationSize / 2) / this.scale / this.scale && localPosX > 0 && !actor.hasFilter()) {
          // actor without filter outside right
          this.removeActor(actor);
      } else if (localPosX * localPosX + localPosY * localPosY > (this.activationSize / 2) * (this.activationSize / 2) / this.scale / this.scale && localPosX < 0 && actor.hasFilter()) {
          // actor with filter outside left
          this.removeActor(actor);
      } else if (localPosX * localPosX + localPosY * localPosY > (this.ripOffSize / 2) * (this.ripOffSize / 2) / this.scale / this.scale) {
          // actor outside rip-off
          this.removeActor(actor);
      }

      if (this.nearActors.isEmpty()) {
        // no more actors/activating actor is gone
        this.setVolumeInPercent(0f);
        this.mute();
      }
    }
  }
  
  public void updateVolume() {
    Iterator<Actor> iteratorActors = this.nearActors.iterator();
      while (iteratorActors.hasNext()) {
        Actor actor = iteratorActors.next();
        if (!actor.hasFilter()) {
          // no filter -> check
          int localPosX = actor.getPosX() - this.posX;
          int localPosY = actor.getPosY() - this.posY;
          if (localPosX < 0 && localPosX * localPosX + localPosY * localPosY > (this.activationSize / 2) * (this.activationSize / 2) / this.scale / this.scale) {
            // on volume selector
            if (localPosY == 0) {
              this.setVolumeInPercent(66.666666);
            } else if (localPosX == 0) {
              this.setVolumeInPercent(0);
            } else {
              // high = 90 - 135 deg
              // mid = 135 - 225 deg
              // low = 225 - 270 deg
              float mathX = localPosX;
              float mathY = -localPosY;
              if (abs(mathY) > abs(mathX) && mathY > 0) {
                // high
                this.setVolumeInPercent(0);
              } else if (abs(mathY) > abs(mathX) && mathY < 0) {
                // low
                float angle = atan(mathX / mathY) * 180 / PI;
                this.setVolumeInPercent(map(angle, 45, 10, 30.43479, 0));
              } else {
                // mid
                float angle = atan(mathY / mathX) * 180 / PI;
                this.setVolumeInPercent(map(angle, -35, 45, 100, 30.43479));
              }
            }
            return;
          }
        }
      }
  }
  
  public void updateFilters() {
    if (!this.hasNearActorWithFilter() && this.patchedFilter != null) {
      // unpatch
      this.lpf.setFilterNoEffect();
      this.hpf.setFilterNoEffect();
      this.del.setFilterNoEffect();
      this.fla.setFilterNoEffect();
      this.bit.setFilterNoEffect();
      this.patchedFilter = null;
    } else if (this.hasNearActorWithFilter() && this.patchedFilter == null) {
      // patch filter
      Iterator<Actor> iteratorActors = this.nearActors.iterator();
      while (iteratorActors.hasNext()) {
        Actor actor = iteratorActors.next();
        if (actor.hasFilter()) {
          Filter newFilter = actor.getFilter();
          if (newFilter.getFilterType() == 0) {
            this.lpf.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
            this.patchedFilter = this.lpf;
          } else if (newFilter.getFilterType() == 1) {
            this.hpf.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
            this.patchedFilter = this.hpf;
          } else if (newFilter.getFilterType() == 2) {
            this.del.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
            this.patchedFilter = this.del;
          } else if (newFilter.getFilterType() == 3) {
            this.fla.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
            this.patchedFilter = this.fla;
          } else if (newFilter.getFilterType() == 4) {
            this.bit.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
            this.patchedFilter = this.bit;
          }
          return;
        }
      }
    } else if (this.hasNearActorWithFilter() && this.patchedFilter != null) {
      // change existing filter
      Iterator<Actor> iteratorActors = this.nearActors.iterator();
      while (iteratorActors.hasNext()) {
        Actor actor = iteratorActors.next();
        if (actor.hasFilter()) {
          Filter newFilter = actor.getFilter();
          if (newFilter.getFilterType() == 0 && newFilter.getFilterType() == this.patchedFilter.getFilterType()) {
            this.lpf.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
          } else if (newFilter.getFilterType() == 1 && newFilter.getFilterType() == this.patchedFilter.getFilterType()) {
            this.hpf.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
          } else if (newFilter.getFilterType() == 2) {
            this.del.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
          } else if (newFilter.getFilterType() == 3) {
            this.fla.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
          } else if (newFilter.getFilterType() == 4) {
            this.bit.setFilterValue(actor.getPosX() - this.posX, actor.getPosY() - this.posY, this.activationSize, this.ripOffSize);
          }
          return;
        }
      }
    }
  }
  
  protected void addNewActor(Actor actor) {
    if (this.nearActors.size() >= 1 && !actor.hasFilter()) return; // if actor has no filter then don't add except activator
    if (this.getActor(actor.getCursorId()) == null) {
      this.nearActors.add(actor);
    }
  }
  
  protected Actor getActor(int cursorId) {
    Iterator<Actor> iterator = nearActors.iterator();
    while (iterator.hasNext()) {
      Actor actor = iterator.next();
      if (actor.getCursorId() == cursorId) {
        return actor;
      }
    }
    return null;
  }
  
  protected void removeActor(Actor actor) {
    this.nearActors.remove(getActor(actor.getCursorId()));
  }
  
  protected boolean hasNearActorWithFilter() {
    Iterator<Actor> iterator = nearActors.iterator();
    while (iterator.hasNext()) {
      Actor actor = iterator.next();
      if (actor.hasFilter()) {
        return true;
      }
    }
    return false;
  }
  
  public void draw() { 
    // draw volume selector
    if (this.isPlaying()) {
      // outer circle
      float maxSize = this.ripOffSize / this.scale / 2;
      float minSize = this.activationSize / this.scale / 2 + (this.ripOffSize / this.scale / 2 - this.activationSize / this.scale / 2) / 3;
      float startMax = HALF_PI * 1.5;
      float endMin = 3 * HALF_PI;
      float angleDegStep = 1f;
      noFill();
      strokeWeight(5);
      float angle = HALF_PI;
      float angleEnd = 3 * HALF_PI;
      float sizeRadius = minSize;
      float colorStartMaxR = 164;
      float colorStartMaxG = 31;
      float colorStartMaxB = 193;
      float colorEndMinR = 16;
      float colorEndMinG = 3;
      float colorEndMinB = 19;
      float colorR = colorEndMinR;
      float colorG = colorEndMinG;
      float colorB = colorEndMinB;
      float xInner;
      float xOuter;
      float yInner;
      float yOuter;
      while (angle <= angleEnd) {
        if (angle < startMax) {
          // muted area
          sizeRadius = this.activationSize / this.scale / 2 + (this.ripOffSize / this.scale / 2 - this.activationSize / this.scale / 2) / 16 * 7;
        } else {
          sizeRadius = map(angle, startMax, endMin, maxSize, minSize);
          colorR = map(angle, startMax, endMin, colorStartMaxR, colorEndMinR);
          colorG = map(angle, startMax, endMin, colorStartMaxG, colorEndMinG);
          colorB = map(angle, startMax, endMin, colorStartMaxB, colorEndMinB);
        }
        xInner = this.posX + minSize * sin(angle + HALF_PI);
        yInner = this.posY + minSize * cos(angle + HALF_PI);
        xOuter = this.posX + sizeRadius * sin(angle + HALF_PI);
        yOuter = this.posY + sizeRadius * cos(angle + HALF_PI);
        stroke(colorR, colorG, colorB);
        line(xInner, yInner, xOuter, yOuter);
        angle += angleDegStep / 180 * PI;
      }
      
      // draw volume selector - inner circle
      strokeWeight(2);
      stroke(164, 31, 193);
      arc(this.posX, this.posY, minSize * 2, minSize * 2, HALF_PI, 3 * HALF_PI);
      
      // muted sign
      noStroke();
      tint(164, 31, 193);
      if (this.muted == null) {
        this.muted = loadImage("muted.png");
      }
      image(this.muted, this.posX + (minSize + maxSize) / 2 * sin(2.25 * HALF_PI) - this.mutedSize / 2 / this.scale, this.posY + (minSize + maxSize) / 2 * cos(2.25 * HALF_PI) - this.mutedSize / 2 / this.scale, this.mutedSize / this.scale, this.mutedSize / this.scale);
      
      // draw rip-off for effects
      if (this.hasNearActorWithFilter()) {
        strokeWeight(2);
        stroke(124, 197, 118);
        noFill();
        line(this.posX, this.posY + minSize, this.posX, this.posY + this.ripOffSize / this.scale / 2);
        line(this.posX, this.posY - minSize, this.posX, this.posY - this.ripOffSize / this.scale / 2);
        // outer circle
        arc(this.posX, this.posY, this.ripOffSize / this.scale, this.ripOffSize / this.scale, -HALF_PI, HALF_PI);
        // inner circle
        arc(this.posX, this.posY, minSize * 2, minSize * 2, -HALF_PI, HALF_PI);
      }
    }
    
    // draw activation circle
    noStroke();
    fill(0, 51, 102, 90);
    ellipse(this.posX, this.posY, this.activationSize / this.scale, this.activationSize / this.scale);
    
    // draw wave
    drawWave();    
    
    // draw active status on wall
    if (this.isPlaying()) {
      noStroke();
      fill(164, 31, 193);
    } else {
      noStroke();
      fill(190, 165, 217);
    }
    float posDifferenceY = 2000 / this.scale;
    float posDifferenceX = 80f / this.scale;
    if (this.lowerRow) {
      posDifferenceY = 2200 / this.scale;
      posDifferenceX = 360 / this.scale;
    }
    ellipse(this.posX + posDifferenceX - 150 / this.scale, this.posY - posDifferenceY, this.activationSize / this.scale * 1.5f, this.activationSize / this.scale * 1.5f);
    noStroke();
    fill(0, 26, 51);
    ellipse(this.posX + posDifferenceX - 150 / this.scale, this.posY - posDifferenceY, this.activationSize / this.scale * 4f / 5f * 1.5f, this.activationSize / this.scale * 4f / 5f * 1.5f);
    fill(0, 0, 0);
    
    // draw information about the track on wall
    fill(191, 208, 226);
    String informationText = "";
    if (this.playing) {
      informationText += "Aktiv\n";
      informationText += "Volume: " + (int)this.volumeInPercent + "%\n";
    } else {
      informationText += "Inaktiv\n";
    }
    String filterName = "";
    Iterator<Actor> iterator = nearActors.iterator();
    while (iterator.hasNext()) {
      Actor actor = iterator.next();
      if (actor.hasFilter()) {
        filterName = "Filter: " + actor.getFilter().getFilterName() + "\n";      
        if (actor.getFilter().getFilterType() == 0) {
          filterName += this.lpf.getFilterParameters();
        } else if (actor.getFilter().getFilterType() == 1) {
          filterName += this.hpf.getFilterParameters();
        } else if (actor.getFilter().getFilterType() == 2) {
          filterName += this.del.getFilterParameters();
        } else if (actor.getFilter().getFilterType() == 3) {
          filterName += this.fla.getFilterParameters();
        } else if (actor.getFilter().getFilterType() == 4) {
          filterName += this.bit.getFilterParameters();
        }
      }
    }
    informationText += filterName;
    textFont(fontGeneral, 48 / this.scale);
    text(informationText, this.posX + posDifferenceX - 150 / this.scale, this.posY - posDifferenceY + 180 / this.scale);
    
    // draw icon on ground and wall
    if (!this.isPlaying()) {
      tint(190, 165, 217);
    } else {
      tint(164, 31, 193);
    }
    image(this.icon, this.posX - (this.iconSize / 2) / this.scale, this.posY - (this.iconSize / 2) / this.scale, this.iconSize / this.scale, this.iconSize / this.scale);
    image(this.icon, this.posX + posDifferenceX - 150 / this.scale - (this.iconSize / 2) / this.scale, this.posY - (this.iconSize / 2) / this.scale - posDifferenceY, this.iconSize / this.scale, this.iconSize / this.scale);
  }
  
  public void updateRotationWave(float deltaTime) {
    this.waveAngle += waveAngleVel * deltaTime;
  }
  
  protected void drawWave() {
    if (this.isPlaying()) {
      stroke(164, 31, 193);
    } else {
      stroke(190, 165, 217);
    }
    strokeWeight(1);
    float waveAngleDistance = 2 * PI / this.waveAmount;
    float waveAngleWidth = waveAngleDistance / 3 * 2;
    float waveAngleSteps = 4f * PI / 180;
    float tempWaveAngle = this.waveAngle;
    float x1;
    float x2 = -1;
    float y1;
    float y2 = -1;
    while (tempWaveAngle <= waveAngleWidth + this.waveAngle - waveAngleSteps) {
      for (int waveId = 0; waveId < this.waveAmount; waveId++) {
        int bufferPos1 = (int)map(tempWaveAngle, this.waveAngle, this.waveAngle + waveAngleWidth, 0, this.out.bufferSize());
        if (bufferPos1 < 0) bufferPos1 = 0;
        if (bufferPos1 >= this.out.bufferSize()) bufferPos1 = this.out.bufferSize() - 1;
        float amp1 = this.out.left.get(bufferPos1) * 1.5;
        if (amp1 < 0) amp1 = 0;
        if (amp1 > 1) amp1 = 1;
        int bufferPos2 = (int)map(tempWaveAngle + waveAngleSteps, this.waveAngle, this.waveAngle + waveAngleWidth, 0, this.out.bufferSize());
        if (bufferPos2 < 0) bufferPos2 = 0;
        if (bufferPos2 >= this.out.bufferSize()) bufferPos2 = this.out.bufferSize() - 1;
        float amp2 = this.out.left.get(bufferPos2) * 1.5;
        if (amp2 < 0) amp2 = 0;
        if (amp2 > 1) amp2 = 1;
        x1 = this.posX + (this.waveDist + waveMaxAmpInPixels * amp1) * sin(tempWaveAngle + waveId * waveAngleDistance);
        y1 = this.posY + (this.waveDist + waveMaxAmpInPixels * amp1) * cos(tempWaveAngle + waveId * waveAngleDistance);
        x2 = this.posX + (this.waveDist + waveMaxAmpInPixels * amp2) * sin(tempWaveAngle + waveId * waveAngleDistance);
        y2 = this.posY + (this.waveDist + waveMaxAmpInPixels * amp2) * cos(tempWaveAngle + waveId * waveAngleDistance);
        if (x1 != -1) {
          line(x1, y1, x2, y2);
        }
      }
      tempWaveAngle += waveAngleSteps;
    }
  }
  
}