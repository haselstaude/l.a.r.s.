class BarVisualizer {
  
  protected int posX = 930 / scale; 
  protected int posYGroundLine = 255 / scale;
  protected int heightVis = 200 / scale;
  protected int barAmount = 40;
  protected int barWidth = 40 / scale;
  protected int barDist = 10 / scale;
  
  protected ArrayList<Track> tracks;
  
  
  public BarVisualizer() {
    this.tracks = new ArrayList<Track>();
  }
  
  public void addTrackReference(Track track) {
    this.tracks.add(track);
  }
  
  public void draw() {
    // calc amount running tracks and gather signal
    float[] totalSignal = new float[this.barAmount];
    int runningTracks = 0;
    float avgVolume = 0;
    
    Iterator<Track> iterator = this.tracks.iterator();
    while (iterator.hasNext()) {
      Track track = iterator.next();
      if (track.isPlaying()) {
        runningTracks++;
        avgVolume += track.getVolumeInPercent();
        
        int bufferPosBegin;
        for (int bar = 0; bar < this.barAmount; bar++) {
          bufferPosBegin = (int)map(bar, 0, this.barAmount, 0, track.getAudioOutput().bufferSize() - 1);
          for (int bufferPos = bufferPosBegin; bufferPos < bufferPosBegin + track.getAudioOutput().bufferSize() / this.barAmount; bufferPos++) {
            totalSignal[bar] += abs(track.getAudioOutput().left.get(bufferPos));
          }
        }
      }
    }
    
    // break down total
    float maxValue = 0;
    for (int bar = 0; bar < this.barAmount; bar++) {
      if (totalSignal[bar] > maxValue) {
        maxValue = totalSignal[bar];
      }
    }
    // mix volume
    avgVolume /= runningTracks;
    for (int bar = 0; bar < this.barAmount; bar++) {
      totalSignal[bar] /= maxValue;
      totalSignal[bar] *= avgVolume / 100;
    }
    
    // draw
    int xBar, size;
    fill(191, 208, 226);
    for (int bar = 0; bar < this.barAmount; bar++) {
      xBar = this.posX + (this.barWidth + this.barDist) * bar;
      size = (int)(totalSignal[bar] * heightVis);
      rect(xBar, this.posYGroundLine - size, this.barWidth, size);
    }
  }
}